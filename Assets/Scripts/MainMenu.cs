using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject menu;

    private void Awake()
    {
        Time.timeScale = 0;
    }
    public void LoadScene()
    {
        menu.SetActive(false);
        Time.timeScale = 1;
    }
}
