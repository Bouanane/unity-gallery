using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disactivate : MonoBehaviour
{
    public GameObject intro;
    void Start()
    {
        StartCoroutine(Disactive());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator Disactive()
    {
        yield return new WaitForSeconds(6);
        intro.SetActive(false);
    }
}
